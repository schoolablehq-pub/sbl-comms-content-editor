const path = require("path");
const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");
module.exports = {
  entry: path.resolve(__dirname, "../src/CommsContentEditor.vue"),
  output: {
    path: path.resolve(__dirname, "../dist"),
    publicPath: "/",
    filename: "comms-content-editor.min.js",
    libraryTarget: "umd",
    library: "CommsContentEditor"
  },
  devtool: "#source-map",
  resolve: {
    extensions: [".json", ".js", ".vue"],
    alias: {
      components: path.resolve(__dirname, "../src/components"),
      "@": path.resolve(__dirname, "../src/"),
      vue$: "vue/dist/vue.esm.js"
    }
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV)
    }),

    new ExtractTextPlugin({
      filename: "comms-content-editor.min.css"
    }),

    new UglifyJSPlugin({
      sourceMap: true,

      compress: {
        warnings: false,
        drop_debugger: true,
        drop_console: true,
        screw_ie8: true,
        global_defs: {
          "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV)
        }
      },

      mangle: {
        screw_ie8: true
      },

      output: {
        comments: false,
        screw_ie8: true
      }
    }),

    new webpack.optimize.OccurrenceOrderPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: "css-loader"
        })
      },
      {
        test: /\.vue$/,
        loader: "vue-loader",
        options: {
          preserveWhitespace: false,
          postcss: [
            require("autoprefixer")({
              browsers: ["last 10 versions", "ie 11"]
            }),
            require("cssnano")({
              discardComments: {
                removeAll: true
              },
              safe: true
            })
          ],

          loaders: {
            scss:
              "vue-style-loader!css-loader!sass-loader?minimize?{discardComments:{removeAll:true}}"
          }
        }
      },
      {
        test: /\.js$/,
        use: "babel-loader",
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        use: {
          loader: "url-loader",
          options: {
            limit: 10000,
            name: "images/[name].[hash:7].[ext]"
          }
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        use: {
          loader: "url-loader",
          options: {
            limit: 10000,
            name: "fonts/[name].[hash:7].[ext]"
          }
        }
      }
    ]
  }
};
