# sbl-comms-content-editor

ContentEditor for comms

# Requirements

- [Vue.js](https://github.com/yyx990803/vue) `^2.0.0`

# Installation

## add repository to npm dependencies

```shell
  "dependencies": {
    "sbl-comms-content-editor": "https://gitlab.com/schoolablehq-pub/sbl-comms-content-editor.git#v1.1.0",
  }
```

# Usage

```html
<template>
  <div class="card">
    <div class="row">
      <div class="form-group">
        <label for>Content</label>
        <comms-content-editor
          :base-api-url="baseApiUrl"
          ref="contentForm"
        ></comms-content-editor>
      </div>
    </div>
  </div>
</template>
<script>
  import CommsContentEditor from "sbl-comms-content-editor";

  export default {
      components: {
          CommsContentEditor
      }
    data() {
      return {
        topic: {},
        baseApiUrl: "SchoolableApiDevUrl",
      };
    },
    methods: {
        onSubmit(data) {
            this.topic.body_content = this.$refs.contentEditor.getContentItems();
        },
    }
  };
</script>
```

# License

[The MIT License](http://opensource.org/licenses/MIT)
